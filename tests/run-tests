#!/bin/bash

set -e
set -x

copy_apks_into_repo() {
    for f in `ls -1 ../../*/bin/*.apk`; do
        name=$(basename $(dirname `dirname $f`))
        echo "name $name"
        apk=`aapt d badging "$f" | sed -n "s,^package: name='\(.*\)' versionCode='\([0-9][0-9]*\)' .*,\1_\2.apk,p"`
        echo "apk $apk"
        cp -f $f $1/repo/$apk
    done
}

if [ -z $WORKSPACE ]; then
    WORKSPACE=`dirname $(pwd)`
    echo "Setting Workspace to $WORKSPACE"
fi

# allow the location of the script to be overridden
if [ -z $fdroid ]; then
    fdroid="$WORKSPACE/fdroid"
fi

#------------------------------------------------------------------------------#
# setup a new repo from scratch

REPOROOT=`mktemp --directory --tmpdir=$WORKSPACE`
cd $REPOROOT
$fdroid init
copy_apks_into_repo $REPOROOT
$fdroid update -c
$fdroid update


#------------------------------------------------------------------------------#
# setup a new repo from scratch and generate a keystore

REPOROOT=`mktemp --directory --tmpdir=$WORKSPACE`
KEYSTORE=$REPOROOT/keystore.jks
cd $REPOROOT
$fdroid init --keystore $KEYSTORE
test -e $KEYSTORE
copy_apks_into_repo $REPOROOT
$fdroid update -c
$fdroid update
test -e repo/index.xml
test -e repo/index.jar


#------------------------------------------------------------------------------#
# setup a new repo from scratch with a HSM/smartcard

REPOROOT=`mktemp --directory --tmpdir=$WORKSPACE`
cd $REPOROOT
$fdroid init --keystore NONE
test -e opensc-fdroid.cfg
test ! -e NONE
